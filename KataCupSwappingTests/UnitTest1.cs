using System;
using Xunit;

namespace KataCupSwappingTests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("C", new string[] { "AB", "CA" })]
        [InlineData("C", new string[] { "AB", "CA", "AB" })]
        // this row is buged?
        [InlineData("B", new string[] { "AC", "CA", "CA", "AC" })]
        [InlineData("A", new string[] { "BA", "AC", "CA", "BC" })]
        [InlineData("A", new string[] { "BC", "CB", "CA", "BA" })]
        [InlineData("C", new string[] { "BC" })]
        [InlineData("B", new string[] { "BA", "CA", "CB", "CA" })]
        [InlineData("B", new string[] { "B" })]
        public void TestCupSwapping(string expected, string[] input)
        {
            //Arrange
            string actual;
            //Act
            actual = KataCupSwapping.Program.CupSwap(input);
            //Assert
            Assert.Equal(expected, actual);

        }


        //		Assert.AreEqual(Program.CupSwapping(new String[] { "AB", "CA" }), "C", $"Test {i++}");
        //		Assert.AreEqual(Program.CupSwapping(new String[] { "AB", "CA", "AB" }), "C", $"Test {i++}");
        //Assert.AreEqual(Program.CupSwapping(new String[] { "AC", "CA", "CA", "AC" }), "B", $"Test {i++}");
        //Assert.AreEqual(Program.CupSwapping(new String[] { "BA", "AC", "CA", "BC" }), "A", $"Test {i++}");
        //Assert.AreEqual(Program.CupSwapping(new String[] { "BC", "CB", "CA", "BA" }), "A", $"Test {i++}");
        //Assert.AreEqual(Program.CupSwapping(new String[] { "BC" }), "C", $"Test {i++}");
        //Assert.AreEqual(Program.CupSwapping(new String[] { "BA", "CA", "CB", "CA" }), "B", $"Test {i++}");
        //Assert.AreEqual(Program.CupSwapping(new String[] { }), "B", $"Test {i++}");
    }
}
